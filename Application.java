public class Application {
	
	public static void main(String[] args) {
		Student[] people = new Student[5];
		people[0] = new Student("132645", "Person 1", "Comp. Sci.");
		people[1] = new Student("684315", "Person 2", "Comp. Sci.");
		people[2] = new Student("931570", "Person 3", "Comp. Sci.");
		people[3] = new Student("305496", "Person 4", "Comp. Sci.");
		people[4] = new Student("642598", "Person 5", "Comp. Sci.");
		
		Classroom javaTwo = new Classroom("Java II", 567, people);
		System.out.println(javaTwo.toString());
	}
}