public class Classroom {
	
	private String subject;
	private int code;
	private Student[] students;
	
	public Classroom(String subject, int code, Student[] st) {
		this.subject = subject;
		this.code = code;
		this.students = st;
	}
	
	public String getSubject() {
		return this.subject;
	}
	public int getCode() {
		return this.code;
	}
	public Student[] getStudents() {
		return this.students;
	}
	
	public String toString() {
		String s = "";
		
		for (int i = 0; i < students.length; i++)
		{
			s = s + this.students[i];
		}
		
		return (this.subject + ": " + this.code + ", " + s);
	}
}